from tkinter import ttk
from tkinter import *
import mariadb
class palabras:
    def __init__(self, window):
        self.wind = window
        self.wind.title('Slang panameños') 
        frame = LabelFrame(self.wind, text = 'Agrega nuevas palabras')
        frame.grid(row = 0, column = 0, columnspan = 3, pady = 20)
        Label(frame, text = 'Definicion: ').grid(row = 1, column = 0)
        self.palabras = Entry(frame)
        self.palabras.focus()
        self.palabras.grid(row = 1, column = 1)
        Label(frame, text = 'Palabras: ').grid(row = 2, column = 0)
        self.definicion = Entry(frame)
        self.definicion.grid(row = 2, column = 1)
        ttk.Button(frame, text = 'GUARDADO', command = self.add_palabras).grid(row = 3, columnspan = 2, sticky = W + E)
        self.message = Label(text = '', fg = 'black')
        self.message.grid(row = 3, column = 0, columnspan = 2, sticky = W + E)
        self.tree = ttk.Treeview(height = 10, columns = 2)
        self.tree.grid(row = 4, column = 0, columnspan = 2)
        self.tree.heading('#0', text = 'definicion', anchor = CENTER)
        self.tree.heading('#1', text = 'palabras', anchor = CENTER)
        ttk.Button(text = 'DELETE', command = self.delete_palabras).grid(row = 5, column = 0, sticky = W + E)
        ttk.Button(text = 'EDIT', command = self.edit_palabras).grid(row = 5, column = 1, sticky = W + E)
        self.get_palabras()
    def run_query(self, query):
        try:
            conn=mariadb.connect(
                host="localhost",
                user="root",
                password="root",
                database="slang"
            )
        except mariadb.Error as e:
            print("Error al conectarse a la bd ",e)
        cur=conn.cursor()
        cur.execute(query)
        return cur
    def get_palabras(self,where=""):
        records = self.tree.get_children()
        for element in records:
            self.tree.delete(element)
        if len(where)>0:
            cur=self.run_query("SELECT `nombre`,`palabras` FROM `slang` "+where)
        else:
            cur=self.run_query("SELECT `palabras`,`definicion` FROM `slang`")

        query = 'SELECT * FROM slang ORDER BY palabras ASC'
        db_rows = self.run_query(query)
        
        for row in db_rows:
            self.tree.insert('', 0, text = row[1], values = row[2])
    def validation(self):
        return len(self.palabras.get()) != 0 and len(self.definicion.get()) != 0
    def add_palabras(self):
        if self.validation():
            query="INSERT INTO `slang` (`id`, `palabras`, `definicion`) VALUES (NULL, '"+self.palabras.get()+"', '"+self.definicion.get()+"');"
            self.run_query(query)
            self.message['text'] = 'palabra {} agregada correctamente'.format(self.palabras.get())
            self.palabras.delete(0, END)
            self.definicion.delete(0, END)
        else:
            self.message['text'] = 'Por favor introduzca los datos'
        self.get_palabras()
    def delete_palabras(self):
        self.message['text'] = ''
        try:
           self.tree.item(self.tree.selection())['text'][0]
        except IndexError as e:
            self.message['text'] = 'Por favor seleccione el registro que quiere eliminar'
            return
        self.message['text'] = ''
        palabras = self.tree.item(self.tree.selection())['text']
        query="delete from slang where palabras='"+self.palabras.get()+"' "
        self.run_query(query,)
        self.message['text'] = 'La palabra {} fue elminado exitosamente'.format(palabras)
        self.get_palabras()
    def edit_palabras(self):
        self.message['text'] = ''
        try:
            self.tree.item(self.tree.selection())['values'][0]
        except IndexError as e:
            self.message['text'] = 'Por favor seleccione un dato'
            return
        palabras = self.tree.item(self.tree.selection())['text']
        vieja_definicion = self.tree.item(self.tree.selection())['values'][0]
        self.edit_wind = Toplevel()
        self.edit_wind.title = 'Editar'
        Label(self.edit_wind, text = 'Vieja palabra:').grid(row = 0, column = 1)
        Entry(self.edit_wind, textvariable = StringVar(self.edit_wind, value = palabras), state = 'readonly').grid(row = 0, column = 2)
        Label(self.edit_wind, text = 'Nueva palabra:').grid(row = 1, column = 1)
        nuevas_palabras = Entry(self.edit_wind)
        nuevas_palabras.grid(row = 1, column = 2)
        Label(self.edit_wind, text = 'Vieja definicion:').grid(row = 2, column = 1)
        Entry(self.edit_wind, textvariable = StringVar(self.edit_wind, value = vieja_definicion), state = 'readonly').grid(row = 2, column = 2)
        Label(self.edit_wind, text = 'Nueva definicion:').grid(row = 3, column = 1)
        nueva_definicion= Entry(self.edit_wind)
        nueva_definicion.grid(row = 3, column = 2)
        Button(self.edit_wind, text = 'Actualizar', command = lambda: self.edit_records(nuevas_palabras.get(), palabras, nueva_definicion.get(), vieja_definicion)).grid(row = 4, column = 2, sticky = W)
        self.edit_wind.mainloop()
    def edit_records(self, nuevas_palabras, palabras, nueva_definicion, vieja_definicion):
        query="UPDATE slang set palabras='"+self.palabras.get()+"',definicion='"+self.definicion.get()+"' where definicion='"+self.definicion.get()+"'; "
        parameters = (nuevas_palabras, nueva_definicion,palabras, vieja_definicion)
        self.run_query(query)
        self.edit_wind.destroy()
        self.message['text'] = 'La palabra {} fue actualizado exitosamente'.format(palabras)
        self.get_palabras()

if __name__ == '__main__':
    window = Tk()
    application = palabras(window)
    application.get_palabras()
    window.mainloop()
